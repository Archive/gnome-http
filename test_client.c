#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include "ghttp.h"

char *date_1 = "Sun, 06 Nov 1994 08:49:37 GMT";
char *date_2 = "Sunday, 06-Nov-94 08:49:37 GMT";
char *date_3 = "Sun Nov  6 08:49:37 1994";
char *date_4 = "Sunday, 06-Nov-03 08:49:37 GMT";

void test_uri_parser(char *a_string);

void test_date_parser(char *a_string);

int
main (int argc, char **argv)
{
        ghttp_request *req;
        ghttp_status status;
        ghttp_current_status curstat;
        char **header_names;
        int num_headers;
        int i;

        if (argc != 2) {
                printf ("usage: %s <url>\n", argv[0]);
                exit (1);
        }

        req = ghttp_request_new ();
        if (req == NULL) {
                printf ("Failed to create new ghttp request.\n");
                exit (1);
        }

        if (ghttp_enable_ssl (req) != 0) {
                printf ("(SSL not supported.)\n");
        }

        if (ghttp_uri_validate (argv[1]) != 0) {
                printf ("URI validation failed for: %s\n", argv[1]);
                exit (1);
        }

        if (ghttp_set_uri (req, argv[1]) != 0) {
                printf ("URI validation failed x2 for: %s\n", argv[1]);
                exit (1);
        }
        ghttp_set_header (req, http_hdr_User_Agent, "GNOME-http/1.0");

        if (ghttp_set_type (req, ghttp_type_get) != 0) {
                printf ("Failed to set GET mode.\n");
                exit (1);
        }
        if (ghttp_set_sync (req, ghttp_async) != 0) {
                printf ("Failed to set async mode.\n");
                exit (1);
        }

        if (ghttp_prepare (req) != 0) {
                printf ("Failed to prepare request.\n");
                exit (1);
        }

        printf ("*** Sending request for: %s\n", argv[1]);

        /* now wait for response */
        while ((status = ghttp_process (req)) == ghttp_not_done) {
                curstat = ghttp_get_status (req);

                switch (curstat.proc) {
                case ghttp_proc_none:
                        printf ("*** (No status.)\n");
                        break;
                case ghttp_proc_request:
                        printf ("*** Status: sending request...\n");
                        break;
                case ghttp_proc_response_hdrs:
                        printf ("*** Status: reading response headers...\n");
                        break;
                case ghttp_proc_response:
                        printf ("*** Status: reading response...\n");
                        break;
                default:
                        printf ("*** Status: unknown! (%d)\n", curstat.proc);
                }

                if (curstat.bytes_total > 0) {
                        printf ("***     Read %d bytes of %d\n", curstat.bytes_read, curstat.bytes_total);
                } else if (curstat.bytes_read > 0) {
                        printf ("***     Read %d bytes\n", curstat.bytes_read);
                }
        }

        if (status == ghttp_error) {
                printf ("*** ERROR: %s\n", ghttp_get_error (req));
                exit (1);
        }

        /* show headers */
        if (ghttp_get_header_names (req, &header_names, &num_headers) != 0) {
                printf ("*** Failed to get header names.\n");
                exit (1);
        }
        printf ("*** Headers:\n");
        for (i = 0; i < num_headers; i++) {
                printf ("***     %s: %s\n", header_names[i], ghttp_get_header (req, header_names[i]));
                free (header_names[i]);
        }
        free (header_names);

        curstat = ghttp_get_status (req);
        printf ("*** HTTP status: %d\n", ghttp_status_code (req));
        printf ("*** Body length %d\n", ghttp_get_body_len (req));
        ghttp_request_destroy (req);

        return 0;
}

#if 0
void
test_uri_parser(char *a_string)
{
  http_uri *uri = NULL;
  uri = http_uri_new();

  printf("Parsing:\n\t%s\n", a_string);
  if (http_uri_parse(a_string, uri) < 0)
    {
      printf("Failed to parse uri.\n");
      return;
    }
  else
    {
      printf("\thost: %s\n", uri->host);
      printf("\tport: %d\n", uri->port);
      printf("\tresource: %s\n", uri->resource);
    }
  http_uri_destroy(uri);
  return;
}

void
test_date_parser(char *a_string)
{
  time_t l_date;

  printf("%s = ", a_string);
  l_date = http_date_to_time(a_string);
  if (l_date < 0)
    printf("Invalid date\n");
  else
    printf("%s\n", ctime(&l_date));
}
#endif
